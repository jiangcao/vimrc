" set the runtime path to include Vundle and initialize
 set rtp+=~/.vim/bundle/Vundle.vim
 call vundle#begin()
" " alternatively, pass a path where Vundle should install plugins
" "call vundle#begin('~/some/path/here')
"
" " let Vundle manage Vundle, required
 Plugin 'VundleVim/Vundle.vim'

 Plugin 'dhruvasagar/vim-table-mode'

 " All of your Plugins must be added before the following line
 call vundle#end()            " required



syn on                      "语法支持

"common conf {{             通用配置
set ai                      "自动缩进
set bs=2                    "在insert模式下用退格键删除
set showmatch               "代码匹配
set laststatus=2            "总是显示状态行
set expandtab               "以下三个配置配合使用，设置tab和缩进空格数
set shiftwidth=4
set tabstop=4
set cursorline              "为光标所在行加下划线
set number                  "显示行号
set autoread                "文件在Vim之外修改过，自动重新读入
set smartindent
filetype plugin indent on
syntax enable
set background=dark
set ignorecase              "检索时忽略大小写
set fileencodings=uft-8,gbk "使用utf-8或gbk打开文件
set hls                     "检索时高亮显示匹配项
set helplang=en             "帮助系统设置为中文
set foldenable
set foldmethod=syntax       "代码折叠
set foldcolumn=0
setlocal foldlevel=1
let fortran_fold = 1
let fortran_do_enddo = 1
let fortran_more_precise = 1

" Save ctrl-b 
 nnoremap <C-b> :w<enter>
 inoremap <C-b> <Esc>:w<enter>

"conf for tabs, 为标签页进行的配置，通过ctrl h/l切换标签等
let mapleader = ','
nnoremap <C-l> gt
nnoremap <C-h> gT
nnoremap <leader>t : tabe<CR>

"conf for buffer
nnoremap <F5> :buffers<CR>:buffer<Space>

" Make ctrl-m
 nnoremap <C-m> :!make clean; make<enter>

" gnuplot current script ctrl-g
 nnoremap <C-g> :!gnuplot %:p<enter>

" open the file under cursor ctrl-x
 nnoremap <C-x>  :!xdg-open %:p:h/<cfile><CR>
